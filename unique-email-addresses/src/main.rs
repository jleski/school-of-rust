/* 929. Unique Email Addresses
====================================================================================================

Every email consists of a local name and a domain name, separated by the @ sign. For example, in
alice@leetcode.com, alice is the local name, and leetcode.com is the domain name.

Besides lowercase letters, these emails may contain '.'s or '+'s.

If you add periods ('.') between some characters in the local name part of an email address, mail
sent there will be forwarded to the same address without dots in the local name.  For example,
"alice.z@leetcode.com" and "alicez@leetcode.com" forward to the same email address.  (Note that
this rule does not apply for domain names.)

If you add a plus ('+') in the local name, everything after the first plus sign will be ignored.
This allows certain emails to be filtered, for example m.y+name@email.com will be forwarded to
my@email.com.  (Again, this rule does not apply for domain names.)

It is possible to use both of these rules at the same time.

Given a list of emails, we send one email to each address in the list.  How many different
addresses actually receive mails?

Example 1:

Input: ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
Output: 2
Explanation: "testemail@leetcode.com" and "testemail@lee.tcode.com" actually receive mails

Note:

1 <= emails[i].length <= 100
1 <= emails.length <= 100
Each emails[i] contains exactly one '@' character.

====================================================================================================
*/

use std::collections::HashSet;

fn count_unique_emails(emails: &Vec<String>) -> i32 {
    let mut unique = HashSet::new();
    for email in emails.iter() {
        unique.insert(format!("{}{}",
            (email[..email.find("@").unwrap()][..email.find("+").unwrap()]
                .replace(".",""))
                .to_string(),
            (email[email.find("@").unwrap()..])
                .to_string())
        );
    }
    println!("{:?} actually receives emails", unique); // for debugging only
    return unique.len() as i32;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_two_unique_emails() {
        let input: Vec<String> = vec![
            // 1st unique mailbox
            "unique1+alias1@example.com".to_string(),
            "unique1+alias2@example.com".to_string(),

            // 2nd unique mailbox
            "unique2+alias1@example.com".to_string()
            ];
        let expect = 2; // how many unique mailboxes / email addresses we expect?
        assert_eq!(count_unique_emails(&input), expect)
    }

    #[test]
    fn test_count_four_unique_emails() {
        let input: Vec<String> = vec![
            // 1st unique mailbox
            "unique1+alias1@example.com".to_string(),
            "unique1+alias2@example.com".to_string(),

            // 2nd unique mailbox
            "unique2+alias1@example.com".to_string(),

            // 3rd unique mailbox
            "unique3+alias2@example.com".to_string(),
            "unique3+alias1@example.com".to_string(),

            // 4th unique mailbox
            "unique4+alias1@example.com".to_string(),
            "unique4+alias1@example.com".to_string()
        ];
        let expect = 4; // how many unique mailboxes / email addresses we expect?
        assert_eq!(count_unique_emails(&input), expect)
    }

    #[test]
    fn test_empty_email_list() {
        let input: Vec<String> = vec![];
        let expect = 0; // how many unique mailboxes / email addresses we expect?
        assert_eq!(count_unique_emails(&input), expect)
    }

}

fn main() {
    let emails: Vec<String> = vec!["test.email+alex@leetcode.com".to_string(),"test.e.mail+bob.cathy@leetcode.com".to_string(),"testemail+david@lee.tcode.com".to_string()];
    println!("Input: emails = \"{:?}\"\nOutput: {}", emails, count_unique_emails(&emails));
}
