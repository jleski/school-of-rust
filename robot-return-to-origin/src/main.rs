/*  657. Robot Return to Origin
    https://leetcode.com/problems/robot-return-to-origin/

    There is a robot starting at position (0, 0), the origin, on a 2D plane. Given a sequence of its
    moves, judge if this robot ends up at (0, 0) after it completes its moves.

    The move sequence is represented by a string, and the character moves[i] represents its ith
    move. Valid moves are R (right), L (left), U (up), and D (down). If the robot returns to the
    origin after it finishes all of its moves, return true. Otherwise, return false.

    Note: The way that the robot is "facing" is irrelevant. "R" will always make the robot move to
    the right once, "L" will always make it move left, etc. Also, assume that the magnitude of the
    robot's movement is the same for each move.

    Example 1:
    Input: "UD"
    Output: true
    Explanation: The robot moves up once, and then down once. All moves have the same magnitude, so
    it ended up at the origin where it started. Therefore, we return true.

    Example 2:
    Input: "LL"
    Output: false
    Explanation: The robot moves left twice. It ends up two "moves" to the left of the origin. We
    return false because it is not at the origin at the end of its moves.
*/

/* School of Rust: Robot Return to Origin
** Author: Jaakko Leskinen <jaakko.leskinen@gmail.com>
*/

impl Solution {
    pub fn judge_circle(moves: String) -> bool {

    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_move_up_and_down_is_true() {
        assert_eq!(judge_circle("UD"), true)
    }

    #[test]
    fn test_move_left_right_is_true() {
        assert_eq!(judge_circle("LR"), true)
    }

    #[test]
    fn test_move_updownleftrightdownup_is_true() {
        assert_eq!(judge_circle("UDLRU"), true)
    }

    #[test]
    fn test_move_leftleft_is_false() {
        assert_eq!(judge_circle("LL"), false)
    }

    #[test]
    fn test_move_leftdownrightup_is_true() {
        assert_eq!(judge_circle("LDRULDRULDRU"), true)
    }

    #[test]
    fn test_move_invalid_direction() {
        let result = std::panic::catch_unwind(|| judge_circle("DDRN"));
        assert!(result.is_err());
    }

}

fn main() {
    println!("{:#?}", judge_circle( "UD"));
    println!("{:#?}", judge_circle( "LL"));
    println!("{:#?}", judge_circle( "UDLRDU"));
}
