// You're given strings J representing the types of stones that are jewels, and
// S representing the stones you have.  Each character in S is a type of stone
// you have.  You want to know how many of the stones you have are also jewels.

// The letters in J are guaranteed distinct, and all characters in J and S are
// letters. Letters are case sensitive, so "a" is considered a different type of
// stone from "A".

// Example 1:
// Input: J = "aA", S = "aAAbbbb"
// Output: 3

// Example 2:
// Input: J = "z", S = "ZZ"
// Output: 0

// Note:
// S and J will consist of letters and have length at most 50.
// The characters in J are distinct.

// https://leetcode.com/problems/jewels-and-stones/

fn count_jewels(j: String, s: String) -> i32 {
    let mut count = 0;
    for c in s.chars() {
        if j.contains(c) {
            count += 1;
        }
    }
    return count;
}

fn main() {
    let (mut j, mut s) = ("aA".to_string(), "aAAbbbb".to_string());
    println!("Input: J = \"{}\", S = \"{}\"\nOutput: {}", &j,&s, count_jewels(j.clone(),s.clone()));
    j = "z".to_string();
    s = "ZZ".to_string();
    println!("Input: J = \"{}\", S = \"{}\"\nOutput: {}", &j,&s, count_jewels(j.clone(),s.clone()));
}
